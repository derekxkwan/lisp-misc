;;;; args: length of list, max step, number of lists

(defun random-l (n lst maxstep)
  (cond
    ((null lst) '())
    ((= (length lst) n) lst)
    (t (let ((updown (random 101))
	     (step (random maxstep)))
	 (cond ((> updown 50) (random-l n (append lst (list (+ (car (last lst)) (+ 1 step)))) maxstep))
	       (t (random-l n (append lst (list (- (car (last lst)) (+ 1 step)))) maxstep)))))
    ))

(let ((cur-n (parse-integer (nth 1 *posix-argv*)))
      (cur-maxstep (parse-integer (nth 2 *posix-argv*)))
      (num-times (max 1(parse-integer (nth 3 *posix-argv*)))))
  (loop for x from 1 to num-times do
       (print (random-l cur-n '(0) cur-maxstep)))
  (format t "~%"))
